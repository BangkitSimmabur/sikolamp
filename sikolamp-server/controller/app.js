var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();

var path = require('path');

var cors = require("cors")
var cor = cors();
app.use(cor);
app.use(express.static(path.join(__dirname, "../public")));

var sikolamp = require('../model/sikolamp.js');

app.get('/api/getuser1/:user', function (req, res) {
    var username= req.params.user;
    sikolamp.getUser(username, function (err, result) {
        if (!err) {
            res.send(result);
        }
        else {
            console.log(err);
            res.status(500).send(err);
        }
    });
    
});

app.get('/api/getuser2/:user', function (req, res) {
    var username= req.params.user;
    sikolamp.getUser1(username, function (err, result) {
        if (!err) {
            res.send(result);
        }
        else {
            console.log(err);
            res.status(500).send(err);
        }
    });
    
});

app.get('/api/getlampu/:user', function (req, res) {
    var username= req.params.user;
    sikolamp.getLampu(username, function (err, result) {
        if (!err) {
            res.send(result);
        }
        else {
            console.log(err);
            res.status(500).send(err);
        }
    });
    
});

app.post('/api/addlampu', urlencodedParser, jsonParser, function (req, res) {
    var id_lampu = req.body.id_lampu;
    var status_lampu = req.body.status_lampu;
    var status_fitur = req.body.status_fitur;
    var username  = req.body.username;
   

sikolamp.addLampu(id_lampu, status_lampu, status_fitur, username, function (err, result ){
    if (!err) {
        console.log(result);
        res.send(result.affectedRows + ' record ditambahkan');
    }
    else {
        console.log(err);
        res.status(500).send(err.code);
    }
})
})

app.post('/api/adduser', urlencodedParser, jsonParser, function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var hak_akses = req.body.hak_akses;
    
   

sikolamp.addUser(username, password, hak_akses, function (err, result ){
    if (!err) {
        console.log(result);
        res.send(result.affectedRows + ' record ditambahkan');
    }
    else {
        console.log(err);
        res.status(500).send(err.code);
    }
})
})

app.post('/api/lampu_update/:id_lampu', urlencodedParser, jsonParser, function (req, res) {
    var id_lampu = req.params.id_lampu;
    var status_lampu = req.body.status_lampu;
   
    sikolamp.updateLampu(status_lampu, id_lampu, function(err, result) {
        if (!err) {
            console.log(result);
            res.send(result.affectedRows + ' record diubah');
        } else {
            console.log(err);
            res.status(500).send(err.code);
        }
    });

});

app.post('/api/fitur_update/:id_lampu', urlencodedParser, jsonParser, function (req, res) {
    var id_lampu = req.params.id_lampu;
    var status_fitur = req.body.status_fitur;
   
    sikolamp.updateFitur(status_fitur, id_lampu, function(err, result) {
        if (!err) {
            console.log(result);
            res.send(result.affectedRows + ' record diubah');
        } else {
            console.log(err);
            res.status(500).send(err.code);
        }
    });

});

app.delete('/api/lampu_del/:id_lampu', function (req, res) {
    var id_lampu = req.params.id_lampu;

    sikolamp.deleteLampu(id_lampu, function (err, result) {
        if (!err) {
            console.log(result);
            res.send(result.affectedRows + ' record dihapus ');
        } else {
            console.log(err);
            res.status(500).send(err.code);
        }
    });
});

app.delete('/api/user_del/:username', function (req, res) {
    var username = req.params.username;

    sikolamp.deleteUser(username, function (err, result) {
        if (!err) {
            console.log(result);
            res.send(result.affectedRows + ' record dihapus ');
        } else {
            console.log(err);
            res.status(500).send(err.code);
        }
    });
});

module.exports = app 
