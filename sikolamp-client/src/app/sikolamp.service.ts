import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class SikolampService {

  constructor(
    public http: Http,
    public toastController: ToastController
  ) { }
  LoadLampu(username){
    return this.http.get('http://localhost:8081/api/getlampu/'+ username);
  }
  getadmin(username){
    return this.http.get('http://localhost:8081/api/getuser1/'+ username);
  }
  gethak(username){
    return this.http.get('http://localhost:8081/api/getuser2/'+ username);
  }
}
