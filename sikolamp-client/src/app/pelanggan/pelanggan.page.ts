import { Component, OnInit } from '@angular/core';
import { SikolampService } from '../sikolamp.service';
import { ModalController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

@Component({
  selector: 'app-pelanggan',
  templateUrl: './pelanggan.page.html',
  styleUrls: ['./pelanggan.page.scss'],
})
export class PelangganPage implements OnInit {
  
  listlampu: any ;
  constructor(
    public sikolampService: SikolampService,
    public modalController: ModalController,
    public router: Router,
    public route: ActivatedRoute
  ) { }

  ngOnInit() {
    var username = localStorage.getItem('local_storage');
    console.log(username);
    this.sikolampService.LoadLampu(username).subscribe((response: Response)=>{
      let data = response.json();
      this.listlampu = data;
    });
    console.log(this.listlampu);
  }
  
  logout(){
    console.log("Berhasil Logout");
    this.modalController.dismiss();

  }
}
