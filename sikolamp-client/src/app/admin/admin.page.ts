import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActionSheetController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {

  constructor(
    public router:Router,
    public actionSheetController: ActionSheetController,
    public modalController: ModalController
  ) { }

  ngOnInit() {
    var username = localStorage.getItem('local_storage');
    console.log(username);
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'User',
      buttons: [{
        text: 'Delete User',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'User Detail',
        icon: 'contact',
        handler: () => {
          console.log('User Detail clicked');
          this.detailuser();
        }
      }, {
        text: 'Edit User',
        icon: 'Create',
        handler: () => {
          console.log('Edit User clicked');
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  logout(){
    console.log("Berhasil Logout");
    this.modalController.dismiss();
  }
  detailuser(){
    this.router.navigate(['/user-detail'])
  }
  goAddUser(){
    this.router.navigate(['/adduser'])
  }
}
