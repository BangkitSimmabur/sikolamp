import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { PelangganPage } from './pelanggan/pelanggan.page';
import { PelangganPageModule } from './pelanggan/pelanggan.module';
import { HttpModule } from '@angular/http';
import { LoginPage } from './login/login.page';
import { LoginPageModule } from './login/login.module';
import { AdminPage } from './admin/admin.page';
import { AdduserPage } from './adduser/adduser.page';
import { AdduserPageModule } from './adduser/adduser.module';
import { AdminPageModule } from './admin/admin.module';
import { UserDetailPage } from './user-detail/user-detail.page';
import { UserDetailPageModule } from './user-detail/user-detail.module';
import { IonicStorageModule } from '@ionic/storage';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [PelangganPage,LoginPage,AdminPage,AdduserPage,UserDetailPage],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    PelangganPageModule,
    AppRoutingModule,
    LoginPageModule,
    AdduserPageModule,
    AdminPageModule,
    UserDetailPageModule,
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
